#!/usr/bin/env python
import sys

from cmd_handler import CmdHandler

from prompt_toolkit import prompt
from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.styles import style_from_dict
from pygments.token import Token

def get_dir():
    return "/root"

def get_host():
    return "localhost"

def get_prompt_tokens(cli):
    return [(Token.Username, 'root'),
        (Token.At,       '@'),
        (Token.Host,     get_host()),
        (Token.Colon,    ':'),
        (Token.Path,     get_dir()),
        (Token.Pound,    '# ')
    ]
    return [(Token.Path, "root@{}:{} # ".format(get_host(), get_dir()))]

class SaltConsole(object):
    cmd_handler = CmdHandler()
    style = style_from_dict({
        # User input.
        Token:          '#ffffff',

        # Prompt.
        Token.Username: '#ffffff bold',
        Token.At:       '#ffffff bold',
        Token.Colon:    '#ffffff bold',
        Token.Pound:    '#ffffff bold',
        Token.Host:     '#ffffff bold',
        Token.Path:     '#ffffff bold',
    })
    history = InMemoryHistory()

    def __init__(self):
        pass

    def handle_input(self, user_input):
        tokens = user_input.strip().split()
        self.cmd_handler.handle(tokens)

    def main_loop(self):
        while True:
            user_input = prompt(
                get_prompt_tokens=get_prompt_tokens,
                style=self.style,
                history=self.history,
                patch_stdout = True
            )
            self.handle_input(user_input)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: {} [minion_id]".format(sys.argv[0])
        sys.exit(1)

    console = SaltConsole()
    console.main_loop()
    # 